var playernumber=1;
var globalscore=[0,0,0];
var dicescore=0;
var activeplayer;
var winnerscore;
const button = document.querySelector('.RollDice');
const button2 = document.querySelector('.hold');
// var actineplayer=1;
document.querySelector('.NewGame').addEventListener('click',function () {
    document.querySelector('.globalScore1').textContent='0';
    document.querySelector('.globalScore2').textContent='0';
    document.querySelector('.dice1').textContent='0';
    document.querySelector('.dice2').textContent='0';
    playernumber=1;
    globalscore=[0,0,0];
    button.disabled = false;
    button2.disabled = false;
});
// user RollDICE
document.querySelector('.RollDice').addEventListener('click',function () {
    Rolldice();

})
function  Rolldice(){
    var randomnumber = Math.floor(Math.random() * 6)+1;
    document.getElementsByClassName("diceimg")[0].src = `./image/dice-${randomnumber}.png`;
    if(randomnumber!=1){
        dicescore+=randomnumber;
        document.querySelector('.dice'+playernumber).textContent=dicescore;
        activeplayer=playernumber;
    }
    else {
        document.querySelector('.dice'+playernumber).textContent='0';
        playernumber = playernumber== 1 ? 2 : 1;
        dicescore=0;
    }
}
document.querySelector('.hold').addEventListener('click',function () {
    Globalscore();
    winner();
    playernumber = playernumber== 1 ? 2 : 1;
})
function Globalscore() {
    globalscore[activeplayer]+=dicescore;
    document.querySelector('.globalScore' + activeplayer).textContent = globalscore[activeplayer];
    document.querySelector('.dice' + activeplayer).textContent = 0;
    dicescore = 0;
}

function winner(winnerscore) {
    winnerscore=document.querySelector('.inputscore').value;
    if(globalscore[activeplayer]>winnerscore) {
        // document.querySelector('.nameplayer'+activeplayer).style.color='red';
        document.querySelector('.nameplayer' + activeplayer).textContent = "Winner!";
        button.disabled = true;
        button2.disabled = true;
    }
}
